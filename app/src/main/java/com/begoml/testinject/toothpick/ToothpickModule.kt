package com.begoml.testinject.toothpick

import com.begoml.testinject.*
import toothpick.config.Module

class ToothpickModule : Module() {

    init {
        bind(INewsInteractor::class.java).to(NewsInteractor::class.java)
        bind(IProfileInteractor::class.java).to(ProfileInteractor::class.java)
        bind(IPaymentInteractor::class.java).to(PaymentInteractor::class.java)

        bind(IRepoNews::class.java).to(RepoNews::class.java)
        bind(IRepoProfile::class.java).to(RepoProfile::class.java)
        bind(IRepoPayment::class.java).to(RepoPayment::class.java)

        bind(IRemoteDataStore::class.java).to(RemoteDataStore::class.java)
        bind(ICacheDataStore::class.java).to(CacheDataStore::class.java)

        bind(INewsApiDataSource::class.java).to(NewsApiDataSource::class.java)
        bind(IProfileApiDataSource::class.java).to(ProfileApiDataSource::class.java)
        bind(IPaymentApiDataSource::class.java).to(PaymentApiDataSource::class.java)

        bind(INewsDbDataSource::class.java).to(NewsDbDataSource::class.java)
        bind(IProfileDbDataSource::class.java).to(ProfileDbDataSource::class.java)
        bind(IPaymentDbDataSource::class.java).to(PaymentDbDataSource::class.java)
    }
}