package com.begoml.testinject

import android.os.Bundle
import android.os.Debug
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.begoml.testinject.dagger.AppComponent
import org.koin.android.ext.android.inject
import toothpick.Toothpick
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var daggerInteractor: INewsInteractor

    private val koinInteractor: INewsInteractor by inject()

    //@Inject lateinit var toothpickInteractor: INewsInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Debug.startMethodTracing("InjectDagger")
        AppComponent.Initializer
            .init((applicationContext as App))
            .inject(this@MainActivity)
        Log.d(javaClass.name, daggerInteractor.getName())
        Debug.stopMethodTracing()

        Debug.startMethodTracing("InjectKoin")
        Log.d(javaClass.name, koinInteractor.toString())
        Debug.stopMethodTracing()


//        Debug.startMethodTracing("InjectToothpick")
//        val appScope = (applicationContext as App).appScope!!
//        Toothpick.openScopes(appScope, "${javaClass.simpleName}_${hashCode()}")
//        appScope.inject(this)
//        Log.d(javaClass.name, toothpickInteractor.toString())
//        Debug.stopMethodTracing()
    }
}