package com.begoml.testinject.dagger

import com.begoml.testinject.*
import dagger.Binds
import dagger.Module

@Module
abstract class DaggerModule {

    @Binds
    abstract fun provideTestInteractor(repo: TestInteractor): ITestInteractor

    @Binds
    abstract fun provideNewsInteractor(repo: NewsInteractor): INewsInteractor

    @Binds
    abstract fun provideProfileInteractor(repo: ProfileInteractor): IProfileInteractor

    @Binds
    abstract fun providePaymentInteractor(repo: PaymentInteractor): IPaymentInteractor

    @Binds
    abstract fun provideRepoNews(repo: RepoNews): IRepoNews

    @Binds
    abstract fun provideRepoProfile(repo: RepoProfile): IRepoProfile

    @Binds
    abstract fun provideRepoPayment(repo: RepoPayment): IRepoPayment

    @Binds
    abstract fun provideRemoteDataStore(repo: RemoteDataStore): IRemoteDataStore

    @Binds
    abstract fun provideCacheDataStore(repo: CacheDataStore): ICacheDataStore

    @Binds
    abstract fun provideNewsApiDataSource(repo: NewsApiDataSource): INewsApiDataSource

    @Binds
    abstract fun provideProfileApiDataSource(repo: ProfileApiDataSource): IProfileApiDataSource

    @Binds
    abstract fun providePaymentApiDataSource(repo: PaymentApiDataSource): IPaymentApiDataSource

    @Binds
    abstract fun provideNewsDbDataSource(repo: NewsDbDataSource): INewsDbDataSource

    @Binds
    abstract fun provideProfileDbDataSource(repo: ProfileDbDataSource): IProfileDbDataSource

    @Binds
    abstract fun providePaymentDbDataSource(repo: PaymentDbDataSource): IPaymentDbDataSource
}
