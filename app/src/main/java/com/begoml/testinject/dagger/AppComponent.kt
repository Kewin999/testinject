package com.begoml.testinject.dagger

import com.begoml.testinject.App
import com.begoml.testinject.MainActivity
import dagger.BindsInstance
import dagger.Component


@Component(
    modules = [
        DaggerModule::class
    ]
)
interface AppComponent {

    fun inject(app: App)
    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun app(app: App): Builder
    }

    class Initializer private constructor() {
        companion object {

            fun init(app: App): AppComponent = DaggerAppComponent.builder()
                .app(app)
                .build()
        }
    }
}