package com.begoml.testinject

import javax.inject.Inject


class TestInteractor @Inject constructor() : ITestInteractor

class NewsInteractor @Inject constructor(val repo: IRepoNews) : INewsInteractor {

    override fun getName(): String {
        return "TestingDagger&Koin"
    }
}

class ProfileInteractor @Inject constructor(val repo: IRepoProfile) : IProfileInteractor
class PaymentInteractor @Inject constructor(val repo: IRepoPayment) : IPaymentInteractor

class RepoNews @Inject constructor(val remote: IRemoteDataStore, val cache: ICacheDataStore) : IRepoNews
class RepoProfile @Inject constructor(val remote: IRemoteDataStore, val cache: ICacheDataStore) : IRepoProfile
class RepoPayment @Inject constructor(val remote: IRemoteDataStore, val cache: ICacheDataStore) : IRepoPayment


class RemoteDataStore @Inject constructor(
    val news: INewsApiDataSource,
    val prfile: IProfileApiDataSource,
    val payment: IPaymentApiDataSource
) : IRemoteDataStore

class CacheDataStore @Inject constructor(
    val news: INewsDbDataSource,
    val prfile: IProfileDbDataSource,
    val payment: IPaymentDbDataSource
) : ICacheDataStore


class NewsApiDataSource @Inject constructor() : INewsApiDataSource
class ProfileApiDataSource @Inject constructor() : IProfileApiDataSource
class PaymentApiDataSource @Inject constructor() : IPaymentApiDataSource


class NewsDbDataSource @Inject constructor() : INewsDbDataSource
class ProfileDbDataSource @Inject constructor() : IProfileDbDataSource
class PaymentDbDataSource @Inject constructor() : IPaymentDbDataSource


interface ITestInteractor

interface INewsInteractor {
    fun getName(): String
}

interface IProfileInteractor
interface IPaymentInteractor

interface IRepoNews
interface IRepoProfile
interface IRepoPayment


interface IRemoteDataStore
interface ICacheDataStore


interface INewsApiDataSource
interface IProfileApiDataSource
interface IPaymentApiDataSource


interface INewsDbDataSource
interface IProfileDbDataSource
interface IPaymentDbDataSource