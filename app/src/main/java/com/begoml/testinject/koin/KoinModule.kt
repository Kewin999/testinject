package com.begoml.testinject.koin

import com.begoml.testinject.*
import org.koin.dsl.module.module

val koinModule = module {

    factory<INewsInteractor> { NewsInteractor(get()) }
    factory<IProfileInteractor> { ProfileInteractor(get()) }
    factory<IPaymentInteractor> { PaymentInteractor(get()) }

    factory<IRepoNews> { RepoNews(get(), get()) }
    factory<IRepoProfile> { RepoProfile(get(), get()) }
    factory<IRepoPayment> { RepoPayment(get(), get()) }

    factory<IRemoteDataStore> { RemoteDataStore(get(), get(), get()) }
    factory<ICacheDataStore> { CacheDataStore(get(), get(), get()) }

    factory<INewsApiDataSource> { NewsApiDataSource() }
    factory<IProfileApiDataSource> { ProfileApiDataSource() }
    factory<IPaymentApiDataSource> { PaymentApiDataSource() }

    factory<INewsDbDataSource> { NewsDbDataSource() }
    factory<IProfileDbDataSource> { ProfileDbDataSource() }
    factory<IPaymentDbDataSource> { PaymentDbDataSource() }

}