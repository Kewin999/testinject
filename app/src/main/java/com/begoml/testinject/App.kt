package com.begoml.testinject

import android.app.Application
import android.os.Debug
import com.begoml.testinject.dagger.AppComponent
import com.begoml.testinject.koin.koinComponent
import com.begoml.testinject.toothpick.ToothpickModule
import org.koin.android.ext.android.startKoin
import toothpick.Scope
import toothpick.Toothpick

class App : Application() {

    var appScope: Scope? = null

    companion object {
        @JvmStatic
        lateinit var daggerComponent: AppComponent
    }
    override fun onCreate() {
        super.onCreate()

        Debug.startMethodTracing("DaggerCreateComponent")
        daggerComponent = createComponent()
        Debug.stopMethodTracing()


        Debug.startMethodTracing("KoinCreateComponent")
        startKoin(this@App, koinComponent)
        Debug.stopMethodTracing()


        Debug.startMethodTracing("ToothpickCreateComponent")
        Toothpick.openScope(ToothpickScope.App).apply {
            installModules(
                ToothpickModule()
            )
            appScope = this
        }
        Debug.stopMethodTracing()

    }

    private fun createComponent(): AppComponent {
        return AppComponent.Initializer.init(this)
    }
}


sealed class ToothpickScope {
    object App
}